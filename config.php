<?php

return [
    'database' => array(
        'db_name' => 'todo_lists',
        'username' => 'root',
        'password' => '',
        'driver' => 'mysql',
        'host' => '127.0.0.1',
        'options' => array(
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
        ),
    ),
    'DEBUG' => true
];
