<?php require 'views/layouts/top.php' ?>
    <header>
        <h1>Tasks</h1>
        <hr>
        <ul><?php
            if (!empty($tasks)) :
                foreach ($tasks as $task) :
        ?>
                <li><strong>Work Name</strong> - <?= $task->work_name??''; ?></li>
                <li><strong>Starting Date</strong> - <?= $task->start_date??''; ?></li>
                <li><strong>Ending date</strong> - <?= $task->end_date??''; ?></li>
                <li><strong>Ending date</strong> -
                    <?php
                    if (isset($task->status)) {
                        if ($task->status === '0') {
                            echo 'Planning)';
                        } elseif ($task->status === '1') {
                            echo 'Complete';
                        } else {
                            echo 'Doing';
                        }
                    }
                    ?>
                </li>
                    <li><strong><a href="/tasks/edit?id=<?= $task->id??''; ?>">Edit</a></strong></li>
                    <li><strong><a href="/tasks/delete?id=<?= $task->id??''; ?>">Delete</a></strong></li>
                <hr>
        <?php
                endforeach;
            else :
                echo 'No result! <hr>';
            endif;
        ?>
        </ul>
    </header>

    <section>
        <h1>Add new Task</h1>
        <form action="/tasks" method="post">
            <input type='text' name="work_name" maxlength="255">
            <input type='date' name="start_date">
            <input type='date' name="end_date" >
            <select name="status">
                <option value="0">Planning</option>
                <option value="1">Complete</option>
                <option value="2">Doing</option>
            </select>
            <input type="submit" value="Submit">
        </form>
    </section>

<?php require 'views/layouts/bottom.php' ?>