<?php require 'views/layouts/top.php' ?>
    <header>
        <h1>Edit page</h1>
    </header>

    <section>
        <h1>Edit Task</h1>
        <form action="/tasks/edit" method="post">
            <input type='text' name="work_name" maxlength="255" value="<?= $data->work_name??''?>">
            <input type='date' name="start_date" value="<?= $data->start_date??''?>">
            <input type='date' name="end_date"  value="<?= $data->end_date??''?>">
            <select name="status">
                <option value="0" <?= $data->status === '0' ? 'selected' : ''?>>Planning</option>
                <option value="1" <?= $data->status === '1' ? 'selected' : ''?>>Complete</option>
                <option value="2" <?= $data->status === '2' ? 'selected' : ''?>>Doing</option>
            </select>
            <input type="submit" value="Submit">
        </form>
    </section>

<?php require 'views/layouts/bottom.php' ?>