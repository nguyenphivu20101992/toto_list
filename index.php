<?php
ini_set('display_errors', 1);

require_once 'vendor/autoload.php';

(Dotenv\Dotenv::create(__DIR__, '.env'))->load();

require 'app/bootstrap.php';
use App\App\Router;
use App\App\Request;

Router::load('routes.php')
    ->direct(Request::uri(), Request::method());
