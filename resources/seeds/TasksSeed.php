<?php


use Phinx\Seed\AbstractSeed;

class TasksSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $this->table('tasks')->truncate();
        $data = [

            [
                'work_name' => 'work name 1',
                'start_date' => '2019/01/01',
                'end_date' => '2019/02/01',
                'status' => '1',
                'created_at' => date('Y-m-d H:i:s'),
            ]

        ];
        $this->table('tasks')->insert($data)->save();

    }
}
