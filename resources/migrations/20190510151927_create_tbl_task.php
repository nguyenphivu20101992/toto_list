<?php

use Phinx\Migration\AbstractMigration;

class CreateTblTask extends AbstractMigration
{
    public function up()
    {
        $this->table('tasks', ['id' => false, 'primary_key' => ['id'], 'engine' => 'InnoDB', 'collation' => 'utf8mb4_unicode_ci'])
            ->addColumn('id', 'biginteger', ['limit' => 20, 'signed' => false, 'identity' => true])
            ->addColumn('work_name', 'string', ['limit' => 255])
            ->addColumn('start_date', 'date')
            ->addColumn('end_date', 'date')
            ->addColumn('status', 'integer', ['limit' => 1, 'default' => 1, 'signed' => false])
            ->addColumn('created_at', 'timestamp', ['default' => 'CURRENT_TIMESTAMP'])
            ->addColumn('updated_at', 'datetime', ['null' => true])
            ->addColumn('deleted_at', 'datetime',['null' => true])
            ->save();
    }

    public function down()
    {
        $this->table('users')->dropForeignKey(['role_id'])->drop();
    }
}
