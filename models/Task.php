<?php
namespace App\Models;

use App\App\App;

class Task
{
    private $table = 'tasks';
    public $description;
    protected $completed;

    public function isComplete()
    {
        return $this->completed;
    }

    public function complete()
    {
        $this->completed = true;
    }

    public function editTask()
    {
        $db = App::get('db');
        $query = $db->db->prepare("select * from {$this->table};");
//        $query->execute();
    }

    public function deleteTask($id)
    {
        $db = App::get('db');
        return $db->delete($this->table, $id);
    }
}
