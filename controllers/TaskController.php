<?php

namespace App\Controllers;

use App\App\App;
use App\Models\Task;

class TaskController
{
    public static function index()
    {
        $tasks = App::get('db')->selectAll('tasks', null);
        $title = 'Tasks';

        return view('tasks.index', compact('tasks', 'title'));
    }

    public static function store()
    {
        // Save the task.
        try {
            App::get('db')->insert(
                'tasks',
                [
                    'work_name' => $_POST['work_name'],
                    'start_date' => $_POST['start_date'],
                    'end_date' => $_POST['end_date'],
                    'status' => $_POST['status']
                ]
            );
        } catch (Exception $e) {
            require "views/500.php";
        }

        // Redirect to tasks.
        return redirect('tasks');
    }

    public static function edit()
    {
        $data = App::get('db')->selectById('tasks', $_GET['id'], null);
        if (empty($data)) {
            return require "views/500.php";
        }
        return view('tasks.edit', compact('data', 'title'));
    }

    public static function doEdit()
    {
        try {
            App::get('db')->update(
                'tasks',
                [
                    'work_name' => $_POST['work_name'],
                    'start_date' => $_POST['start_date'],
                    'end_date' => $_POST['end_date'],
                    'status' => $_POST['status']
                ]
            );
        } catch (Exception $e) {
            require "views/500.php";
        }

        return redirect('tasks');
    }

    public static function delete()
    {
        $taskModel = new Task();
        $result = $taskModel->deleteTask($_GET['id']);
        if (empty($result)) {
            return require "views/500.php";
        }
        return redirect('tasks');
    }
}
