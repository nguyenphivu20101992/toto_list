<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;
require('vendor/autoload.php');


final class TaskTest extends TestCase
{
    private $http;

    public function setUp()
    {
        $this->http = new GuzzleHttp\Client(['base_uri' => '192.168.0.120:10000']);
    }
    public function testAddWorkSuccess(): void
    {
        $response = $this->http->request('GET', 'tasks');
        $this->assertEquals(200, $response->getStatusCode());
    }
}
