<?php

$router->get('', 'PageController@index');
$router->get('about', 'PageController@about');
$router->get('contact', 'PageController@contact');
$router->get('tasks', 'TaskController@index');
$router->post('tasks', 'TaskController@store');
$router->get('tasks/edit', 'TaskController@edit');
$router->get('tasks/delete', 'TaskController@delete');
$router->post('tasks/edit', 'TaskController@doEdit');
