<?php

namespace App\App;

class Request
{
    public static function uri()
    {
        // Requests to /foo?bar=12 should be
        // redirected to /foo
        $parts = parse_url($_SERVER['REQUEST_URI']);
        parse_str($parts['query']??'', $query);
        if (!empty($query)) {
            foreach ($query as $param_k => $param_v) {
                $_GET[$param_k] = $param_v;
            }
        }
        return trim(
            parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH),
            '/'
        );
    }

    public static function method()
    {
        return $_SERVER['REQUEST_METHOD'];
    }
}
