<?php

namespace App\App\Database;

use \PDO;

// A class responsible for database connections.
class Connection
{
    public static function make(array $config)
    {
        try {
            $driver = getenv('DB_DRIVER');
            $host = getenv('DB_HOST');
            $dbname = getenv('DB_NAME');
            $username = getenv('DB_USER');
            $password = getenv('DB_PASS');
            return new PDO(
                "{$driver}:host={$host};dbname={$dbname}",
                $username,
                $password,
                [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]
            );
        } catch (Exception $e) {
            dd($e->getMessage());
        }
    }
}
