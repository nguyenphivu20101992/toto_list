<?php

require_once 'vendor/autoload.php';

(Dotenv\Dotenv::create(__DIR__, '.env'))->load();

return [
    'paths' => [
        'migrations' => '%%PHINX_CONFIG_DIR%%/resources/migrations',
        'seeds' => '%%PHINX_CONFIG_DIR%%/resources/seeds',
    ],
    'environments' => [
        'default_migration_table' => 'migrations',
        'default_database' => 'development',
        'production' => [
            'adapter' => 'mysql',
            'host' => getenv('DB_HOST'),
            'name' => getenv('DB_NAME'),
            'user' => getenv('DB_USER'),
            'pass' => getenv('DB_PASS'),
            'port' => 3306,
            'charset' => 'utf8',
        ],
        'development' => [
            'adapter' => 'mysql',
            'host' => getenv('DB_HOST'),
            'name' => getenv('DB_NAME'),
            'user' => getenv('DB_USER'),
            'pass' => getenv('DB_PASS'),
            'port' => 3306,
            'charset' => 'utf8',
        ],
        'testing' => [
            'adapter' => 'mysql',
            'host' => getenv('DB_HOST_TEST'),
            'name' => getenv('DB_NAME_TEST'),
            'user' => getenv('DB_USER_TEST'),
            'pass' => getenv('DB_PASS_TEST'),
            'port' => 3306,
            'charset' => 'utf8',
        ],
    ],
];
